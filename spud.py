import npyscreen
import configparser
import collections
import time
import threading
import os
import sys
import picamera



class DisplayInfo(object):
    def splash(self, info=''):                     
        screen = ('  ___ ___ _   _ ___      \n'
                  ' / __| _ \ | | |   \     \n'
                  ' \__ \  _/ |_| | |) |    \n'
                  ' |___/_|  \___/|___/     \n'
                  '                         \n'
                  ' Sequential Photograph   \n'
                  ' Uploading Device        \n'
                  '                         \n'
                  ' {} \n'.format(info))
        
        return screen
    
    def info(self, info=''):
        screen = ('SVIS v.0.3.4 \n'
                  'Gillroy Labs \n'
                  '             \n'
                  '{} \n'.format(info))
        return screen

    def convertms(self, ms):
        millis = int(ms)
        dsmillis = millis%1000
        seconds=(millis/1000)%60
        seconds = int(seconds)
        minutes=(millis/(1000*60))%60
        minutes = int(minutes)
        hours=(millis/(1000*60*60))%24
        days=(millis/(1000*60*60*24))
        return "%d Day(s), %02d:%02d:%02d %03d" % (days, hours, minutes, seconds, dsmillis)

class ConfigValues(object):
    def __init__(self, configfile='./config.ini'):
        self.config = configparser.ConfigParser()
        self.cfgfile = configfile
        self.config.read(str(self.cfgfile))
    
    def write(self, setting, value, section='SETTINGS'):
        self.config.read(str(self.cfgfile))
        self.config[section][setting] = value
        with open(self.cfgfile, 'w') as configfile:
            self.config.write(configfile)

    def readstr(self, setting, section='SETTINGS', fallback=0):
        self.config.read(str(self.cfgfile))
        return self.config[section].get(setting, fallback)
    
    def readint(self, setting, section='SETTINGS', fallback=0):
        self.config.read(str(self.cfgfile))
        return self.config[section].getint(setting, fallback)
    
    def readsec(self, section='SETTINGS'):
        self.config.read(str(self.cfgfile))
        return self.config[section]

class StartTimelapse(threading.Thread):
    def __init__(self):
        #self._stopevent = threading.Event()
        super().__init__()
        self.daemon = True

        config = ConfigValues()

        with picamera.PiCamera() as camera:
            camera.awb_mode = config.readstr('whitebalance').lower()
            camera.brightness = config.readint('brightness')
            camera.contrast = config.readint('contrast')
            camera.exposure_mode = config.readstr('exposure').replace(" ", "").lower()
            camera.exposure_compensation = config.readint('ev')
            camera.saturation = config.readint('saturation')
            camera.sharpness = config.readint('sharpness')
            camera.shutter_speed = config.readint('shutterspeed')
            
            if config.readstr('iso') != 'Auto':
                camera.iso = config.readint('iso')
            else:
                camera.iso = 0
    
        self.cycletime = config.readint('cycletime')
        self.frequency = config.readint('frequency')
        self.imgquality = config.readint('quality')
        self.filetype = config.readstr('filetype').lower()
        
        self.dir = './photos'
        self.name = 'image'
        self.timestamp = '{timestamp:%Y-%m-%d_T%H%M%S}'
        self.counter = '{counter:03d}'
        self.namefile = ''.join([self.dir, '/', self.name, '-', self.timestamp, '-', self.counter, '.', self.filetype])

        self._stop_event = threading.Event()

    def run(self):
        try:
            with picamera.PiCamera() as camera:
                try:
                    start = time.time()
                    for i, filename in enumerate(camera.capture_continuous(self.namefile, format=self.filetype, quality=self.imgquality)):
                        time.sleep(self.frequency / 1000.0)
                        if self.stopped():
                            break
                        if time.time() > start + self.cycletime / 1000.0 and self.cycletime != 0:
                            break
                finally:
                    camera.close()
        finally:
            sys.exit()

    def stop(self):
        self._stop_event.set()
    
    def stopped(self):
        return self._stop_event.is_set()

class TakeSnapshot(StartTimelapse):
    def __init__(self):
        super().__init__()
        
        self.dir = '.'
        self.name = 'snapshot'
        self.timestamp = time.time()
        self.namefile = ''.join(['Test', '.',self.filetype])
    
    def run(self):
         with picamera.PiCamera() as camera:
            try:
                camera.capture(self.namefile, format=self.filetype, quality=self.imgquality)
            finally:
                camera.close()

# App
class App(npyscreen.NPSAppManaged):
    def onStart(self):
        self.addForm("MAIN", MainForm, name="Main Menu")
        self.addForm("Preferences", PreferencesForm, name="Preference Menu")
        self.addForm("Advanced", AdvancedForm, name="Advanced Menu")
        self.addForm("Peripherals", PeripheralsForm, name="Peripherals Menu")
        self.addForm("Saturation", SaturationForm, name="Saturation")
        self.addForm("Sharpness", SharpnessForm, name="Sharpness")
        self.addForm("Contrast", ContrastForm, name='Contrast')
        self.addForm("Brightness", BrightnessForm, name='Brightness')
        self.addForm("Quality", QualityForm, name='Quality')
        self.addForm("Ev", EvForm, name='EV')
        self.addForm("Iso", IsoForm, name='ISO')
        self.addForm("Exposure", ExposureForm, name='Exposure')
        self.addForm("Whitebalance", WhitebalanceForm, name='Auto White Balance')
        self.addForm("Filetype", FiletypeForm, name='Filetype')
        self.addForm("Cycletime", CycletimeForm, name='Cycletime')
        self.addForm("Frequency", FrequencyForm, name='Frequency')
        self.addForm("Shutterspeed", ShutterspeedForm, name='Shutterspeed')
        self.addForm("Info", InfoForm, name='Info')
        self.addForm("Timelapse", TimelapseForm, name='Timelapse')
        self.addForm("Preview", PreviewForm, name='Preview')
# Color Theme

# Box Classes
class TextBox(npyscreen.BoxTitle):
    _contained_widget = npyscreen.MultiLineEdit

class Menubox(npyscreen.BoxTitle):
    _contained_widget = npyscreen.MultiLine

class Sliderbox(npyscreen.BoxTitle):
    _contained_widget = npyscreen.SliderNoLabel

# Templates
class MenuForm(npyscreen.FormBaseNew):
    def setup(self):
        self.disp = DisplayInfo()
        self.title = None
        self.default = None
        
        self.options = []
        self.options = collections.OrderedDict(self.options)

    def create(self):
        self.setup()
        y, x = self.useable_space()
        self.menu = self.add(Menubox, name=self.title, values=[*self.options], footer='Arrow Keys & Enter',
                 rely=y // 4, max_width=x // 2 - 5, max_height=y // 2 - 5, scroll_exit=True, color='NO_EDIT', contained_widget_arguments={'exit_right':True})
        self.info = self.add(TextBox, name="Info", footer=None, value=self.default,
                 relx=x // 2, rely=2, max_height=y - 10, editable=False, color='STANDOUT', contained_widget_arguments={'color':'GOOD'})    
        self.select = self.add(npyscreen.Button, name="Select", value=False, relx=3*x // 4 - 3 , min_height=y, editable=False, scroll_exit=True, exit_right=True)
    
    def adjust_widgets(self):
        if self.menu.value != None:
            if self.menu.value >= 0:
                self.select.editable = True
            for i in range(0, len([*self.options])):
                if self.menu.value == i:
                    selection = [*self.options][i]
                    self.select_form(self.options[selection]['Display'], self.options[selection]['Form'])
        else:
            self.select.editable = False
            self.select.value = None
    def select_form(self, message, newform):
        if self.select.value:
            if newform == None:
                sys.exit()
            else:
                self.parentApp.switchForm(newform)
        self.info.value = message
        self.info.display()
    
    def afterEditing(self):
        self.menu.value = None
        self.select.value = None
        self.info.value = self.default

class SliderForm(npyscreen.Form):
    def setup(self):
        self.config = ConfigValues()
        self.disp = DisplayInfo()
        self.title = 'Saturation Values'
        self.setting = 'saturation'
        self.blurb = 'Saturation Setting:'
        self.markings = ['-100', '0', '100']
        self.formnext = 'Advanced'
        self.topval = 200
        self.addto = 100


    def create(self):
        self.setup()
        self.cfgvalue = self.config.readstr(self.setting.lower())
        y, x = self.useable_space()

        self.info = self.add(TextBox, name=self.title, editable=False, max_height=3*y // 8, value=self.disp.info('{} {}'.format(self.blurb, self.cfgvalue)))
        self.nextrelx = 2
        self.endmark = self.add(npyscreen.FixedText, name='endmark', value=self.markings[0], editable=False, max_height=y // 8, max_width=5, rely=y // 2)
        self.nextrelx = x // 2 - 1
        self.midmark = self.add(npyscreen.FixedText, name='midmark', value=self.markings[1], editable=False, max_height=y // 8, max_width=5, rely=y // 2)
        self.nextrelx = -7
        self.endmark = self.add(npyscreen.FixedText, name='endmark', value=self.markings[2], editable=False, max_height=y // 8, max_width=4, rely=y // 2)
        self.nextrelx = 2
        self.sliderval = self.add(npyscreen.SliderNoLabel, name=self.title, max_height=y // 8, max_width=30*x // 32, value=int(self.cfgvalue)+self.addto, out_of=self.topval)
    
    def adjust_widgets(self):
        self.config.write(self.setting, str(int(self.sliderval.value - self.addto)))
        self.cfgvalue = self.config.readstr(self.setting)
        self.info.value = self.disp.info('{} {}'.format(self.blurb, self.cfgvalue))
        self.info.display()
    
    def afterEditing(self):
        if self.formnext == None:
            sys.exit()
        self.parentApp.setNextForm(self.formnext)

class SelectForm(npyscreen.Form):
    def setup(self):
        self.config = ConfigValues()
        self.disp = DisplayInfo()
        self.title = 'ISO Settings'
        self.infodisp = 'ISO Setting:'
        self.setting = 'iso'
        self.formnext = 'Advanced'
        self.options = [('Auto', '0'),
                        ('100', '100'), 
                        ('200', '200'),
                        ('320', '320'),
                        ('400', '400'),
                        ('500', '500'),
                        ('640', '640'),
                        ('800', '800')]

    def create(self):
        self.setup()
        self.options = collections.OrderedDict(self.options)
        self.inverted = {v: k for k, v in self.options.items()}
        y, x = self.useable_space()
        self.cfgvalue = self.config.readstr(self.setting.lower())
        self.info = self.add(TextBox, name=self.title, editable=False, max_height=3*y // 8, value=self.disp.info('{} {}'.format(self.infodisp, self.inverted[self.cfgvalue])))
        self.nextrely += 1
        self.menu = self.add(Menubox, name=self.title, max_height=y // 3, values=[*self.options], scroll_exit=True, contained_widget_arguments={'exit_right':True})
    
    def adjust_widgets(self):
        if self.menu.value != None:
            for i in range(0, len([*self.options])):
                if self.menu.value == i:
                    selection = [*self.options][i]
                    self.config.write(self.setting, self.options[selection])
        self.cfgvalue = self.config.readstr(self.setting)
        self.info.value = self.disp.info('{} {}'.format(self.infodisp, self.inverted[self.cfgvalue]))
        self.info.display()
    
    def afterEditing(self):
        self.parentApp.setNextForm(self.formnext)

class TimeSliderForm(npyscreen.Form):
    def setup(self):
        self.config = ConfigValues()
        self.disp = DisplayInfo()
        self.setting = 'cycletime'
        self.title = 'Cycletime Settings'
        self.infoblurb = 'Timelapse Length:'
        self.cutoff = 0
        self.cutoffdisp = 'Forever Until Shutoff'
        self.formnext = 'Preferences'
        self.increments = [('Milliseconds', 1),
                           ('Centiseconds', 10),
                           ('Seconds', 1000),
                           ('Minutes', 60000), 
                           ('Hours', 3600000), 
                           ('Days', 86400000)]

    def create(self):
        self.setup()
        self.increments = collections.OrderedDict(self.increments)
        y, x = self.useable_space()
        self.cfgvalue = self.disp.convertms(self.config.readint(self.setting))

        self.info = self.add(TextBox, name=self.title, editable=False, max_height=3*y // 8, value=self.disp.info('{} {}'.format(self.infoblurb, self.cfgvalue)))
        self.nextrely += 1
        self.timeinterval = self.add(Menubox, name=self.title, relx=4, max_height=y // 4, max_width=x // 3, value=1, values=[*self.increments], scroll_exit=True, contained_widget_arguments={'exit_right':True})
        self.nextrelx = 2
        self.sliderval = self.add(Sliderbox, name='Use Side Arrow Keys To Adjust Value', max_width=2*x // 3 - 10, max_height=3, contained_widget_arguments={'value':self.config.readint(self.setting), 'step':1000, 'out_of':float('inf')}, rely=y // 2, relx=x // 3 + 5, scroll_exit=True)
    
    def adjust_widgets(self):
        self.config.write(self.setting, str(int(self.sliderval.value)))
        self.cfgvalue = self.disp.convertms(self.config.readint(self.setting))
        if self.timeinterval.value != None:
            for i in range(0, len([*self.increments])):
                if self.timeinterval.value == i:
                    selection = [*self.increments][i]
                    self.sliderval.entry_widget.step = self.increments[selection]
        if self.config.readint(self.setting) <= self.cutoff:
            self.cfgvalue = self.cutoffdisp
        self.info.value = self.disp.info('{} {}'.format(self.infoblurb, self.cfgvalue))
        self.info.display()
    
    def afterEditing(self):
        self.parentApp.setNextForm(self.formnext)

# Menu Forms
class MainForm(MenuForm):
    def setup(self):
        self.disp = DisplayInfo()
        self.title = 'Main Menu'
        self.default = self.disp.splash('29 June 2018\n Build Jank v0.2.1')
        self.option_one = self.disp.splash('Start Timelapse')
        self.option_two = self.disp.splash('Take Preview Snapshot')
        self.option_three = self.disp.splash('Open Preferences Menu')
        self.option_four = self.disp.splash('Open Info Menu')
        self.option_five = self.disp.splash('Exit Program')
        
        self.options = [
          ('Start',{'Display':self.option_one, 'Form': 'Timelapse'}) ,
          ('Preview',{'Display':self.option_two, 'Form': 'Preview'}), 
          ('Preferences',{'Display':self.option_three, 'Form': 'Preferences'}), 
          ('Info',{'Display':self.option_four, 'Form': 'Info'}), 
          ('Exit',{'Display':self.option_five, 'Form': None})]
        self.options = collections.OrderedDict(self.options)
                    
class PreferencesForm(MenuForm):
    def setup(self):
        self.disp = DisplayInfo()
        self.title = 'Preferences Menu'
        self.default = self.disp.splash('Timelapse Preferences')
        self.option_one = self.disp.splash('Change Length of Timelapse')
        self.option_two = self.disp.splash('Change Time Between Photos')
        self.option_three = self.disp.splash('Change Exposure Settings')
        self.option_four = self.disp.splash('Access Peripheral Settings')
        self.option_five = self.disp.splash('Access Advanced Settings')
        self.option_six = self.disp.splash('Return to Main Menu')
        
        self.options = [
          ('Cycletime',{'Display':self.option_one, 'Form': 'Cycletime'}) ,
          ('Frequency',{'Display':self.option_two, 'Form': 'Frequency'}), 
          ('Exposure',{'Display':self.option_three, 'Form': 'Exposure'}), 
          ('Peripherals',{'Display':self.option_four, 'Form': 'Peripherals'}),
          ('Advanced',{'Display':self.option_five, 'Form': 'Advanced'}),
          ('Return',{'Display':self.option_six, 'Form': 'MAIN'})]
        self.options = collections.OrderedDict(self.options)

class AdvancedForm(MenuForm):
    def setup(self):
        self.disp = DisplayInfo()
        self.title = 'Preferences Menu'
        self.default = self.disp.splash('Advanced Settings')
        self.option_one = self.disp.splash('Change Iso Settings')
        self.option_two = self.disp.splash('Change EV Settings')
        self.option_three = self.disp.splash('Change Saturation Settings')
        self.option_four = self.disp.splash('Change Brightness Settings')
        self.option_five = self.disp.splash('Change Contrast Settings')
        self.option_six = self.disp.splash('Change Sharpness Settings')
        self.option_seven = self.disp.splash('Change Quality Settings')
        self.option_eight = self.disp.splash('Change White Balance Settings')
        self.option_nine = self.disp.splash('Change Saved Filetype Settings')
        self.option_ten = self.disp.splash('Change Shutter Speed Settings')        
        self.option_return = self.disp.splash('Return to Preferences Menu')
        
        self.options = [
          ('ISO',{'Display':self.option_one, 'Form': 'Iso'}),
          ('EV',{'Display':self.option_two, 'Form': 'Ev'}), 
          ('Saturation',{'Display':self.option_three, 'Form': 'Saturation'}), 
          ('Brightness',{'Display':self.option_four, 'Form': 'Brightness'}), 
          ('Contrast',{'Display':self.option_five, 'Form': 'Contrast'}),
          ('Sharpness',{'Display':self.option_six, 'Form': 'Sharpness'}), 
          ('Quality',{'Display':self.option_seven, 'Form': 'Quality'}), 
          ('White Balance',{'Display':self.option_eight, 'Form': 'Whitebalance'}), 
          ('File Type',{'Display':self.option_nine, 'Form': 'Filetype'}),
          ('Shutter Speed',{'Display':self.option_ten, 'Form': 'Shutterspeed'}),
          ('Return',{'Display':self.option_return, 'Form': 'Preferences'})]
        self.options = collections.OrderedDict(self.options)

class PeripheralsForm(MenuForm):
    def setup(self):
        self.disp = DisplayInfo()
        self.title = 'Peripherals Menu'
        self.default = self.disp.splash('Peripheral Settings')
        self.option_one = self.disp.splash('Configure I/O 1')
        self.option_two = self.disp.splash('Configure I/O 2')
        self.option_three = self.disp.splash('Configure Stepper Motor')
        self.option_four = self.disp.splash('Return to Preferences Menu')
        
        self.options = [
          ('I/O 1',{'Display':self.option_one, 'Form': None}),
          ('I/O 2',{'Display':self.option_two, 'Form': None}), 
          ('Stepper Motor',{'Display':self.option_three, 'Form': None}), 
          ('Return',{'Display':self.option_four, 'Form': 'Preferences'})]
        self.options = collections.OrderedDict(self.options)    

#Slider Forms
class SaturationForm(SliderForm):
    pass

class SharpnessForm(SliderForm):
    def setup(self):
        self.config = ConfigValues()
        self.disp = DisplayInfo()
        self.title = 'Sharpness Values'
        self.setting = 'sharpness'
        self.blurb = 'Sharpness Setting:'
        self.markings = ['-100', '0', '100']
        self.formnext = 'Advanced'
        self.topval = 200
        self.addto = 100

class ContrastForm(SliderForm):
    def setup(self):
        self.config = ConfigValues()
        self.disp = DisplayInfo()
        self.title = 'Contrast Values'
        self.setting = 'contrast'
        self.blurb = 'Contrast Setting:'
        self.markings = ['-100', '0', '100']
        self.formnext = 'Advanced'
        self.topval = 200
        self.addto = 100

class BrightnessForm(SliderForm):
    def setup(self):
        self.config = ConfigValues()
        self.disp = DisplayInfo()
        self.title = 'Brightness Values'
        self.setting = 'brightness'
        self.blurb = 'Brightness Setting:'
        self.markings = ['0', '50', '100']
        self.formnext = 'Advanced'
        self.topval = 100
        self.addto = 0

class QualityForm(SliderForm):
    def setup(self):
        self.config = ConfigValues()
        self.disp = DisplayInfo()
        self.title = 'Quality Values'
        self.setting = 'quality'
        self.blurb = 'Quality Setting:'
        self.markings = ['0', '50', '100']
        self.formnext = 'Advanced'
        self.topval = 100
        self.addto = 0

class EvForm(SliderForm):
    def setup(self):
        self.config = ConfigValues()
        self.disp = DisplayInfo()
        self.title = 'EV Values'
        self.setting = 'ev'
        self.blurb = 'EV Setting:'
        self.markings = ['-25', '0', '25']
        self.formnext = 'Advanced'
        self.topval = 50
        self.addto = 25

#Select Forms
class IsoForm(SelectForm):
    pass

class ExposureForm(SelectForm):
    def setup(self):
        self.config = ConfigValues()
        self.disp = DisplayInfo()
        self.title = 'Exposure Settings'
        self.infodisp = 'Exposure Setting:'
        self.setting = 'exposure'
        self.formnext = 'Preferences'
        self.options = [('Auto', 'auto'),
                        ('Night', 'night'), 
                        ('Night Preview', 'nightpreview'),
                        ('Backlight', 'backlight'),
                        ('Spotlight', 'spotlight'),
                        ('Sports', 'sports'),
                        ('Snow', 'snow'),
                        ('Beach', 'beach'),
                        ('Very Long', 'verylong'),
                        ('Fixed FPS', 'fixedfps'),
                        ('Anti-Shake', 'antishake'),
                        ('Fireworks', 'fireworks')]

class WhitebalanceForm(SelectForm):
    def setup(self):
        self.config = ConfigValues()
        self.disp = DisplayInfo()
        self.title = 'Auto White Balance Settings'
        self.infodisp = 'Auto White Balance Setting:'
        self.setting = 'whitebalance'
        self.formnext = 'Advanced'
        self.options = [('Auto', 'auto'),
                        ('Sunlight', 'sunlight'), 
                        ('Cloudy', 'cloudy'),
                        ('Shade', 'shade'),
                        ('Tungsten', 'tungsten'),
                        ('Flourescent', 'flourescent'),
                        ('Incandescent', 'incandescent'),
                        ('Flash', 'flash'),
                        ('Horizon', 'horizon')]

class FiletypeForm(SelectForm):
    def setup(self):
        self.config = ConfigValues()
        self.disp = DisplayInfo()
        self.title = 'Filetype Settings'
        self.infodisp = 'Filetype Setting:'
        self.setting = 'filetype'
        self.formnext = 'Advanced'
        self.options = [('JPEG format', 'jpeg'),
                        ('PNG format', 'png'), 
                        ('GIF format', 'gif'),
                        ('Windows Bitmap (BMP)', 'bmp'),
                        ('YUV420 (YUV)', 'yuv'),
                        ('24 bit RGB', 'rgb'),
                        ('32 bit RGBA', 'rgba'),
                        ('24 bit BGR', 'bgr'),
                        ('32 bit BGRA', 'bgra')]

#Time Slider Forms
class CycletimeForm(TimeSliderForm):
    pass

class FrequencyForm(TimeSliderForm):
    def setup(self):
        self.config = ConfigValues()
        self.disp = DisplayInfo()
        self.setting = 'frequency'
        self.title = 'Frequency Settings'
        self.infoblurb = 'Time Between Photos:'
        self.cutoff = 30
        self.cutoffdisp = 'Fast As Possible'
        self.formnext = 'Preferences'
        self.increments = [('Milliseconds', 1),
                           ('Centiseconds', 10),
                           ('Seconds', 1000),
                           ('Minutes', 60000), 
                           ('Hours', 3600000), 
                           ('Days', 86400000)]

class ShutterspeedForm(TimeSliderForm):
    class Microclass(DisplayInfo):
        def convertms(self, ms):
            micro = int(ms)
            dsmicro = micro%1000
            millis = (micro/1000)%1000
            seconds = (micro/(1000*1000))
            return "%03d:%03d:%03d" % (seconds, millis, dsmicro)

    def setup(self):
        self.config = ConfigValues()
        self.disp = self.Microclass()
        self.setting = 'shutterspeed'
        self.title = 'Shutter Speed Settings'
        self.infoblurb = 'Shutter Speed (Seconds:Milli:Micro):'
        self.cutoff = 0
        self.cutoffdisp = 'Auto'
        self.formnext = 'Advanced'
        self.increments = [('1 Microsecond', 1),
                           ('10 Microseconds', 10),
                           ('100 Microseconds', 100), 
                           ('1 Millisecond', 1000), 
                           ('1 Centisecond', 10000),
                           ('1 Decisecond', 100000),
                           ('1 Second', 1000000)]

#Info Screen Forms

class InfoForm(npyscreen.Form):
    def setup(self):
        self.title = 'Camera Preferences'
        self.formnext = 'MAIN'
        self.config = ConfigValues()
    def writescreen(self):
        cfg = self.config.readsec()
        return ['{}: {}'.format(keys, cfg[keys]) for keys in [*cfg]]
    def create(self):
        self.setup()
        self.info = self.add(Menubox, name=self.title, values=self.writescreen(), color='STANDOUT', contained_widget_arguments={'exit_right': True, 'color':'GOOD'})
    def afterEditing(self):
        self.parentApp.setNextForm(self.formnext)
        self.info.values = self.writescreen()
        self.info.display()

# Cleanup Later Forms:

class PreviewForm(npyscreen.Form):
    def create(self):
        self.info = self.add(Menubox, name='Camera Preferences', values=['Preview Shot', 'Hit enter to stop manually'], color='STANDOUT', contained_widget_arguments={'exit_right': True, 'color':'GOOD'})
    
    def beforeEditing(self):
        TakeSnapshot().start()
    
    def afterEditing(self):
        #TakeSnapshot().join()
        self.parentApp.setNextForm('MAIN')

class TimelapseForm(npyscreen.Form):
    def create(self):
        self.info = self.add(Menubox, name='Camera Preferences', values=['Timelapse in progress'], color='STANDOUT', contained_widget_arguments={'exit_right': True, 'color':'GOOD'})
    
    def beforeEditing(self):
        StartTimelapse().start()
    
    def afterEditing(self):
        self.parentApp.setNextForm('MAIN')

if __name__ == "__main__":
    try:
        MyApp = App()
        MyApp.run()
    except KeyboardInterrupt:
        print()
