import npyscreen



class App(npyscreen.NPSAppManaged):
    def onStart(self):
        # Set the theme. DefaultTheme is used by default
        self.addForm("MAIN", MainForm, name="Main Menu")
    
    def splash_screen(self, info=''):
        screen = ('   _______      _______  _____  \n'
                  '  / ____\ \    / /_   _|/ ____| \n'
                  ' | (___  \ \  / /  | | | (___   \n'
                  '  \___ \  \ \/ /   | |  \___ \  \n'
                  '  ____) |  \  /   _| |_ ____) | \n'
                  ' |_____/    \/   |_____|_____/  \n'
                  '                                \n'
                  ' Sequential Vegitation          \n'
                  ' Imaging System                 \n'
                  '                                \n'
                  ' {} \n'.format(info))
        
        return screen
    
    def info_screen(self, info=''):
        screen = ('SVIS v.0.2.1 \n'
                  'Gillroy Labs \n'
                  '             \n'
                  '{} \n'.format(info))
        return screen

    def convertms(self, ms):
        millis = int(ms)
        dsmillis = millis%1000
        seconds=(millis/1000)%60
        seconds = int(seconds)
        minutes=(millis/(1000*60))%60
        minutes = int(minutes)
        hours=(millis/(1000*60*60))%24
        days=(millis/(1000*60*60*24))
        return "%d Day(s), %02d:%02d:%02d %03d" % (days, hours, minutes, seconds, dsmillis)

class TextBox(npyscreen.BoxTitle):
    # MultiLineEdit now will be surrounded by boxing
    _contained_widget = npyscreen.MultiLineEdit

class Menubox(npyscreen.BoxTitle):
    _contained_widget = npyscreen.MultiLine

class Sliderbox(npyscreen.BoxTitle):
    _contained_widget = npyscreen.SliderNoLabel

class MainForm(npyscreen.FormBaseNew):
    def create(self):
        y, x = self.useable_space()
        self.menu = self.add(Menubox, name="Main Menu", values=['Start','Preview', 'Preferences', 'Info', 'Exit'], footer='\u2191\u2193 : Browse  \u21a9 : Info  \u2192 \u21a9 :Select',
                 rely=y // 4, max_width=x // 2 - 5, max_height=y // 2 - 5, scroll_exit=True, color='NO_EDIT', contained_widget_arguments={'exit_right':True})
        self.info = self.add(TextBox, name="Info", footer=None, value=self.parentApp.splash_screen('29 June 2018\n Build Jank v0.2.1'),
                 relx=x // 2, rely=2, max_height=y - 10, editable=False, color='STANDOUT', contained_widget_arguments={'color':'GOOD'})
        self.nextrely += 0
        self.select = self.add(npyscreen.Button, name="Select", relx=3*x // 4 - 3 , min_height=y, editable=False, scroll_exit=True, exit_right=True)
        
    def afterEditing(self):
        self.menu.value = None
        self.select.value = None

    def adjust_widgets(self):
        if self.menu.value != None:
            if self.menu.value >= 0:
                self.select.editable = True
            if self.menu.value == 0:
                self.select_form(self.parentApp.splash_screen('Start Timelapse'), 'Startup')
            elif self.menu.value == 1:
                self.select_form(self.parentApp.splash_screen('Take Preview Snapshot'), None)
            elif self.menu.value == 2:
                self.select_form(self.parentApp.splash_screen('Open Preferences Menu'), 'Preferences')
            elif self.menu.value == 3:
                self.select_form(self.parentApp.splash_screen('Open Info Menu'), 'Info')
            elif self.menu.value == 4:
                exit()
                self.select_form(self.parentApp.splash_screen('Exit Program'), None)
        else:
            self.select.editable = False
            self.select.value = None
    
    def select_form(self, message, newform):
        if self.select.value:
                self.parentApp.switchForm(newform)
        self.info.value = message
        self.info.display()

try:
    MyApp = App()
    MyApp.run()
except KeyboardInterrupt:
    print()