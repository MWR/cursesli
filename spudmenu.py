import npyscreen
import configparser
import subprocess
import time
import threading
import os
import picamera

class ConfigValues(object):
    def __init__(self, configfile='./config.ini'):
        self.config = configparser.ConfigParser()
        self.cfgfile = configfile
        self.config.read(str(self.cfgfile))
    
    def cfgdefault(self, section='SETTINGS'):
        self.config.read(str(self.cfgfile))
        self.config[section] = {'cycletime':0, 'frequency':2000,
        'exposure':'auto','iso':0,'sharpness':0,'saturation':0,
        'brightness':50, 'contrast':0}
        with open(self.cfgfile, 'w') as configfile:
            self.config.write(configfile)
    
    def cfgwrite(self, setting, value, section='SETTINGS'):
        self.config.read(str(self.cfgfile))
        self.config[section][setting] = value
        with open(self.cfgfile, 'w') as configfile:
            self.config.write(configfile)

    def cfgread(self, setting, section='SETTINGS', fallback=0):
        self.config.read(str(self.cfgfile))
        return self.config[section].get(setting, fallback)

class StartTimelapse(threading.Thread, ConfigValues):
    def __init__(self):
        self._stopevent = threading.Event()
        super(StartTimelapse, self).__init__()

    def run(self):
        cfgvals = ConfigValues()
        start = time.time()

        with picamera.PiCamera() as camera:
            camera.awb_mode = cfgvals.cfgread('whitebalance').lower()
            camera.brightness = int(cfgvals.cfgread('brightness'))
            camera.contrast = int(cfgvals.cfgread('contrast'))
            camera.exposure_mode = cfgvals.cfgread('exposure').replace(" ", "").lower()
            camera.exposure_compensation = int(cfgvals.cfgread('ev'))
            camera.saturation = int(cfgvals.cfgread('saturation'))
            camera.sharpness = int(cfgvals.cfgread('sharpness'))
            cycletime = int(cfgvals.cfgread('cycletime'))
            frequency = int(cfgvals.cfgread('frequency'))
            filetype = cfgvals.cfgread('filetype').lower()

            if cfgvals.cfgread('iso') != 'Auto':
                camera.iso = int(cfgvals.cfgread('iso'))
            else:
                camera.iso = 0

            if filetype == 'jpeg':
                imgquality = int(cfgvals.cfgread('quality'))
            else:
                imgquality = None
            
            if frequency < 30:
                frequency = 30
            
            if filetype == 'jpeg':
                namefile = './photos/image{timestamp:%Y-%m-%d_T%H%M%S}-{counter:03d}.jpg'
            elif filetype == 'png':
                namefile = './photos/image{timestamp:%Y-%m-%d_T%H%M%S}-{counter:03d}.png'
            elif filetype == 'gif':
                namefile = './photos/image{timestamp:%Y-%m-%d_T%H%M%S}-{counter:03d}.gif'
            elif filetype == 'bmp':
                namefile = './photos/image{timestamp:%Y-%m-%d_T%H%M%S}-{counter:03d}.bmp'


            try:
                for i, filename in enumerate(camera.capture_continuous(namefile, format=filetype, quality=imgquality)):
                    time.sleep(frequency / 1000.0)
                    if self._stopevent.isSet():
                        break
                    if time.time() > start + cycletime / 1000.0 and cycletime != 0:
                        break
            finally:
                camera.close()
    def join(self, timeout=None):
        self._stopevent.set()
        threading.Thread.join(self, timeout)

class App(npyscreen.NPSAppManaged):
    def onStart(self):
        # Set the theme. DefaultTheme is used by default
        npyscreen.setTheme(SpudTheme)
        self.addForm("MAIN", MainForm, name="Main Menu")
        self.addForm("Preferences", PreferencesForm, name='Preferences')
        self.addForm("Advanced", AdvancedForm, name='Advanced Settings')
        self.addForm("Peripherals", PeripheralsForm, name='Peripherals Settings')
        self.addForm("Sharpness", SharpnessForm, name='Sharpness')
        self.addForm("Saturation", SaturationForm, name='Saturation')
        self.addForm("Contrast", ContrastForm, name='Contrast')
        self.addForm("Brightness", BrightnessForm, name='Brightness')
        self.addForm("Quality", QualityForm, name='Quality')
        self.addForm("Ev", EvForm, name='EV')
        self.addForm("Iso", IsoForm, name='ISO')
        self.addForm("Exposure", ExposureForm, name='Exposure')
        self.addForm("Whitebalance", WhitebalanceForm, name='Auto White Balance')
        self.addForm("Filetype", FiletypeForm, name='File Type')
        self.addForm("LedOne", LedOneForm, name='1st LED')
        self.addForm("LedTwo", LedTwoForm, name='2nd LED')
        self.addForm("Cycletime", CycletimeForm, name='Cycletime')
        self.addForm("Frequency", FrequencyForm, name='Frequency')
        self.addForm("Info", InfoForm, name='Info', color='DEFAULT')
        self.addForm("Startup", StartForm, name='Start')
    
    def splash_screen(self, info=''):
        screen = ('   _______      _______  _____  \n'
                  '  / ____\ \    / /_   _|/ ____| \n'
                  ' | (___  \ \  / /  | | | (___   \n'
                  '  \___ \  \ \/ /   | |  \___ \  \n'
                  '  ____) |  \  /   _| |_ ____) | \n'
                  ' |_____/    \/   |_____|_____/  \n'
                  '                                \n'
                  ' Sequential Vegitation          \n'
                  ' Imaging System                 \n'
                  '                                \n'
                  ' {} \n'.format(info))
        
        return screen
    
    def info_screen(self, info=''):
        screen = ('SVIS v.0.2.1 \n'
                  'Gillroy Labs \n'
                  '             \n'
                  '{} \n'.format(info))
        return screen

    def convertms(self, ms):
        millis = int(ms)
        dsmillis = millis%1000
        seconds=(millis/1000)%60
        seconds = int(seconds)
        minutes=(millis/(1000*60))%60
        minutes = int(minutes)
        hours=(millis/(1000*60*60))%24
        days=(millis/(1000*60*60*24))
        return "%d Day(s), %02d:%02d:%02d %03d" % (days, hours, minutes, seconds, dsmillis)

class SpudTheme(npyscreen.ThemeManager):
    default_colors = {
    'DEFAULT'     : 'WHITE_BLACK',
    'FORMDEFAULT' : 'GREEN_BLACK',
    'NO_EDIT'     : 'BLUE_BLACK',
    'STANDOUT'    : 'CYAN_BLACK',
    'CURSOR'      : 'WHITE_BLACK',
    'CURSOR_INVERSE': 'BLACK_WHITE',
    'LABEL'       : 'GREEN_BLACK',
    'LABELBOLD'   : 'RED_BLACK',
    'CONTROL'     : 'YELLOW_BLACK',
    'IMPORTANT'   : 'GREEN_BLACK',
    'SAFE'        : 'GREEN_BLACK',
    'WARNING'     : 'YELLOW_BLACK',
    'DANGER'      : 'RED_BLACK',
    'CRITICAL'    : 'MAGENTA_BLACK',
    'GOOD'        : 'GREEN_BLACK',
    'GOODHL'      : 'GREEN_BLACK',
    'VERYGOOD'    : 'BLACK_GREEN',
    'CAUTION'     : 'YELLOW_BLACK',
    'CAUTIONHL'   : 'BLACK_YELLOW',
    }


class TextBox(npyscreen.BoxTitle):
    # MultiLineEdit now will be surrounded by boxing
    _contained_widget = npyscreen.MultiLineEdit

class Menubox(npyscreen.BoxTitle):
    _contained_widget = npyscreen.MultiLine

class Sliderbox(npyscreen.BoxTitle):
    _contained_widget = npyscreen.SliderNoLabel

class Pagerbox(npyscreen.BoxTitle):
    _contained_widget = npyscreen.Pager

class Selectbox(npyscreen.BoxTitle):
    _contained_widget = npyscreen.SelectOne

class MainForm(npyscreen.FormBaseNew):
    def create(self):
        y, x = self.useable_space()
        self.menu = self.add(Menubox, name="Main Menu", values=['Start','Preview', 'Preferences', 'Info', 'Exit'], footer='\u2191\u2193 : Browse  \u21a9 : Info  \u2192 \u21a9 :Select',
                 rely=y // 4, max_width=x // 2 - 5, max_height=y // 2 - 5, scroll_exit=True, color='NO_EDIT', contained_widget_arguments={'exit_right':True})
        self.info = self.add(TextBox, name="Info", footer=None, value=self.parentApp.splash_screen('29 June 2018\n Build Jank v0.2.1'),
                 relx=x // 2, rely=2, max_height=y - 10, editable=False, color='STANDOUT', contained_widget_arguments={'color':'GOOD'})
        self.nextrely += 0
        self.select = self.add(npyscreen.MultiLine, name="Options", values=['Select'], color='DEFAULT',
                 relx=3*x // 4 - 3 , min_height=y, editable=False, scroll_exit=True, exit_right=True)
        
    def afterEditing(self):
        self.menu.value = None
        self.select.value = None

    def adjust_widgets(self):
        if self.menu.value != None:
            if self.menu.value >= 0:
                self.select.editable = True
            if self.menu.value == 0:
                self.select_form(self.parentApp.splash_screen('Start Timelapse'), 'Startup')
            elif self.menu.value == 1:
                self.select_form(self.parentApp.splash_screen('Take Preview Snapshot'), None)
            elif self.menu.value == 2:
                self.select_form(self.parentApp.splash_screen('Open Preferences Menu'), 'Preferences')
            elif self.menu.value == 3:
                self.select_form(self.parentApp.splash_screen('Open Info Menu'), 'Info')
            elif self.menu.value == 4:
                exit()
                self.select_form(self.parentApp.splash_screen('Exit Program'), None)
        else:
            self.select.editable = False
            self.select.value = None
    
    def select_form(self, message, newform):
        if self.select.value == 0:
                self.parentApp.switchForm(newform)
        self.info.value = message
        self.info.display()

class PreferencesForm(npyscreen.FormBaseNew):
    def create(self):
        y, x = self.useable_space()
        self.menu = self.add(Menubox, name="Preferences Menu", values=['Cycletime','Frequency', 'Exposure', 'Peripherals','Advanced', 'Return'], 
                 footer='\u2191\u2193 : Browse  \u21a9 : Info  \u2192 \u21a9 :Select', color='NO_EDIT',
                 rely=y // 4, max_width=x // 2 - 5, max_height=y // 2 - 5, scroll_exit=True, contained_widget_arguments={'exit_right':True})
        self.info = self.add(TextBox, name="Info", footer=None, value=self.parentApp.splash_screen('Preferences Menu'),
                 relx=x // 2, rely=2, max_height=y - 10, editable=False, color='NO_EDIT', contained_widget_arguments={'color':'GOOD'})
        self.nextrely += 0
        self.select = self.add(npyscreen.MultiLine, name="Options", values=['Select'],
                 relx=3*x // 4 - 3 , min_height=y, editable=False, scroll_exit=True, exit_right=True)
    
    def afterEditing(self):
        self.DISPLAY()
        self.menu.value = None
        self.select.value = None

    def adjust_widgets(self):
        if self.menu.value != None:
            if self.menu.value >= 0:
                self.select.editable = True
            if self.menu.value == 0:
                self.select_form(self.parentApp.splash_screen('Change Length of Timelapse'), 'Cycletime')
            elif self.menu.value == 1:
                self.select_form(self.parentApp.splash_screen('Change Time Between Settings'), 'Frequency')
            elif self.menu.value == 2:
                self.select_form(self.parentApp.splash_screen('Change Exposures Settings'), 'Exposure')
            elif self.menu.value == 3:
                self.select_form(self.parentApp.splash_screen('Access Peripheral Settings'), 'Peripherals')
            elif self.menu.value == 4:
                self.select_form(self.parentApp.splash_screen('Access Advanced Settings'), 'Advanced')
            elif self.menu.value == 5:
                self.select_form(self.parentApp.splash_screen('Return To Main Menu'), 'MAIN')
        else:
            self.select.editable = False
            self.select.value = None
    
    def select_form(self, message, newform):
        if self.select.value == 0:
                self.parentApp.switchForm(newform)
        self.info.value = message
        self.info.display()

class AdvancedForm(npyscreen.FormBaseNew):
    def create(self):
        y, x = self.useable_space()
        self.menu = self.add(Menubox, name="Advanced Preferences", values=['ISO', 'EV', 'Saturation', 'Brightness', 'Contrast', 'Sharpness', 'Quality', 'White Balance', 'File Type', 'Return'],
                 rely=y // 4, max_width=x // 2 - 5, max_height=y // 2 - 5, scroll_exit=True, contained_widget_arguments={'exit_right':True},
                 footer='\u2191\u2193 : Browse  \u21a9 : Info  \u2192 \u21a9 :Select', color='NO_EDIT')
        self.info = self.add(TextBox, name="Info", footer=None, value=self.parentApp.splash_screen('Preferences Menu'),
                 relx=x // 2, rely=2, max_height=y - 10, editable=False, color='NO_EDIT', contained_widget_arguments={'color':'GOOD'})
        self.nextrely += 0
        self.select = self.add(npyscreen.MultiLine, name="Options", values=['Select'],
                 relx=3*x // 4 - 3 , min_height=y, editable=False, scroll_exit=True, exit_right=True)
    
    def afterEditing(self):
        self.DISPLAY()
        self.menu.value = None
        self.select.value = None

    def adjust_widgets(self):
        if self.menu.value != None:
            if self.menu.value >= 0:
                self.select.editable = True
            if self.menu.value == 0:
                self.select_form(self.parentApp.splash_screen('Change ISO Settings'), 'Iso')
            elif self.menu.value == 1:
                self.select_form(self.parentApp.splash_screen('Change EV Settings'), 'Ev')
            elif self.menu.value == 2:
                self.select_form(self.parentApp.splash_screen('Change Saturation Setting'), 'Saturation')
            elif self.menu.value == 3:
                self.select_form(self.parentApp.splash_screen('Change Brightness Settings'), 'Brightness')
            elif self.menu.value == 4:
                self.select_form(self.parentApp.splash_screen('Change Contrast Setting'), 'Contrast')
            elif self.menu.value == 5:
                self.select_form(self.parentApp.splash_screen('Change Sharpness Setting'), 'Sharpness')
            elif self.menu.value == 6:
                self.select_form(self.parentApp.splash_screen('Change Quality Setting'), 'Quality')
            elif self.menu.value == 7:
                self.select_form(self.parentApp.splash_screen('Change White Balance Setting'), 'Whitebalance')
            elif self.menu.value == 8:
                self.select_form(self.parentApp.splash_screen('Change Saved Filetype'), 'Filetype')
            elif self.menu.value == 9:
                self.select_form(self.parentApp.splash_screen('Return To Preferences'), 'Preferences')
        else:
            self.select.editable = False
            self.select.value = None
        
    def select_form(self, message, newform):
        if self.select.value == 0:
            self.parentApp.switchForm(newform)
        self.info.value = message
        self.info.display()

class PeripheralsForm(npyscreen.FormBaseNew):
    def create(self):
        y, x = self.useable_space()
        self.menu = self.add(Menubox, name="Peripheral Preferences", values=['LED 1', 'LED 2', 'Stepper Motor', 'Return'],
                 rely=y // 4, max_width=x // 2 - 5, max_height=y // 2 - 5, scroll_exit=True, contained_widget_arguments={'exit_right':True},
                 footer='\u2191\u2193 : Browse  \u21a9 : Info  \u2192 \u21a9 :Select', color='NO_EDIT')
        self.info = self.add(TextBox, name="Info", footer=None, value=self.parentApp.splash_screen('Peripherals Menu'),
                 relx=x // 2, rely=2, max_height=y - 10, editable=False, color='NO_EDIT', contained_widget_arguments={'color':'GOOD'})
        self.nextrely += 0
        self.select = self.add(npyscreen.MultiLine, name="Options", values=['Select'],
                 relx=3*x // 4 - 3 , min_height=y, editable=False, scroll_exit=True, exit_right=True)
    
    def afterEditing(self):
        self.DISPLAY()
        self.menu.value = None
        self.select.value = None

    def adjust_widgets(self):
        if self.menu.value != None:
            if self.menu.value >= 0:
                self.select.editable = True
            if self.menu.value == 0:
                self.select_form(self.parentApp.splash_screen('Configure LED 1'), 'LedOne')
            elif self.menu.value == 1:
                self.select_form(self.parentApp.splash_screen('Configure LED 2'), 'LedTwo')
            elif self.menu.value == 2:
                self.select_form(self.parentApp.splash_screen('Configure Stepper Motor'), 'Stepper')
            elif self.menu.value == 3:
                self.select_form(self.parentApp.splash_screen('Return To Preferences'), 'Preferences')
        else:
            self.select.editable = False
            self.select.value = None
    
    def select_form(self, message, newform):
        if self.select.value == 0:
                self.parentApp.switchForm(newform)
        self.info.value = message
        self.info.display()
    
class SaturationForm(npyscreen.Form, ConfigValues):
    def create(self):
        y, x = self.useable_space()
        self.cfgvalue = str(ConfigValues().cfgread('saturation'))

        self.info = self.add(TextBox, name='Saturation Settings', editable=False, max_height=3*y // 8, value=self.parentApp.info_screen('Saturation Setting: {}'.format(self.cfgvalue)))
        self.nextrelx = 1
        self.endmark = self.add(npyscreen.FixedText, name='endmark', value='-100', editable=False, max_height=y // 8, max_width=5, rely=y // 2)
        self.nextrelx = x // 2 - 1
        self.midmark = self.add(npyscreen.FixedText, name='midmark', value='0', editable=False, max_height=y // 8, max_width=5, rely=y // 2)
        self.nextrelx = -7
        self.endmark = self.add(npyscreen.FixedText, name='endmark', value='100', editable=False, max_height=y // 8, max_width=4, rely=y // 2)
        self.nextrelx = 2
        
    
    def adjust_widgets(self):
        ConfigValues().cfgwrite('saturation', str(int(self.sliderval.value - 100)))
        self.cfgvalue = str(ConfigValues().cfgread('saturation'))
        self.info.value = self.parentApp.info_screen('Saturation Setting: {}'.format(self.cfgvalue))
        self.info.display()
    
    def afterEditing(self):
        self.parentApp.setNextForm('Advanced')

class SharpnessForm(npyscreen.Form):
    def create(self):
        y, x = self.useable_space()
        self.cfgvalue = str(ConfigValues().cfgread('sharpness'))

        self.info = self.add(TextBox, name='Sharpness Settings', editable=False, max_height=3*y // 8, value=self.parentApp.info_screen('Sharpness Setting: {}'.format(self.cfgvalue)))
        self.nextrelx = 2
        self.endmark = self.add(npyscreen.FixedText, name='endmark', value='-100', editable=False, max_height=y // 8, max_width=5, rely=y // 2)
        self.nextrelx = x // 2 - 1
        self.midmark = self.add(npyscreen.FixedText, name='midmark', value='0', editable=False, max_height=y // 8, max_width=5, rely=y // 2)
        self.nextrelx = -7
        self.endmark = self.add(npyscreen.FixedText, name='endmark', value='100', editable=False, max_height=y // 8, max_width=4, rely=y // 2)
        self.nextrelx = 2
        self.sliderval = self.add(npyscreen.SliderNoLabel, name='Sharpness Info', max_height=y // 8, max_width=30*x // 32, value=int(self.cfgvalue)+100, out_of=200)
    
    def adjust_widgets(self):
        ConfigValues().cfgwrite('sharpness', str(int(self.sliderval.value - 100)))
        self.cfgvalue = str(ConfigValues().cfgread('sharpness'))
        self.info.value = self.parentApp.info_screen('Sharpness Setting: {}'.format(self.cfgvalue))
        self.info.display()
    
    def afterEditing(self):
        self.parentApp.setNextForm('Advanced')

class ContrastForm(npyscreen.Form):
    def create(self):
        y, x = self.useable_space()
        self.cfgvalue = str(ConfigValues().cfgread('contrast'))

        self.info = self.add(TextBox, name='Contrast Settings', editable=False, max_height=3*y // 8, value=self.parentApp.info_screen('Contrast Setting: {}'.format(self.cfgvalue)))
        self.nextrelx = 1
        self.endmark = self.add(npyscreen.FixedText, name='endmark', value='-100', editable=False, max_height=y // 8, max_width=5, rely=y // 2)
        self.nextrelx = x // 2 - 1
        self.midmark = self.add(npyscreen.FixedText, name='midmark', value='0', editable=False, max_height=y // 8, max_width=5, rely=y // 2)
        self.nextrelx = -7
        self.endmark = self.add(npyscreen.FixedText, name='endmark', value='100', editable=False, max_height=y // 8, max_width=4, rely=y // 2)
        self.nextrelx = 2
        self.sliderval = self.add(npyscreen.SliderNoLabel, name='Contrast Info', max_height=y // 8, max_width=30*x // 32, value=int(self.cfgvalue)+100, out_of=200)
    
    def adjust_widgets(self):
        ConfigValues().cfgwrite('contrast', str(int(self.sliderval.value - 100)))
        self.cfgvalue = str(ConfigValues().cfgread('contrast'))
        self.info.value = self.parentApp.info_screen('Contrast Setting: {}'.format(self.cfgvalue))
        self.info.display()
    
    def afterEditing(self):
        self.parentApp.setNextForm('Advanced')

class BrightnessForm(npyscreen.Form):
    def create(self):
        y, x = self.useable_space()
        self.cfgvalue = str(ConfigValues().cfgread('brightness'))
class IsoForm(npyscreen.Form):
    def create(self):
        y, x = self.useable_space()
        self.cfgvalue = str(ConfigValues().cfgread('iso'))
        self.info = self.add(TextBox, name='ISO Settings', editable=False, max_height=3*y // 8, value=self.parentApp.info_screen('ISO Setting: {}'.format(self.cfgvalue)))
        self.nextrely += 1
        self.menu = self.add(Menubox, name="ISO Setting Selection", max_height=y // 3, values=['Auto','100','200','320', '400', '500', '640', '800'], scroll_exit=True, contained_widget_arguments={'exit_right':True})
    
    def adjust_widgets(self):
        if self.menu.value == 0:
            writevalue = 'Auto'
        elif self.menu.value == 1:
            writevalue = '100'
        elif self.menu.value == 2:
            writevalue = '200'
        elif self.menu.value == 3:
            writevalue = '320'
        elif self.menu.value == 4:
            writevalue = '400'
        elif self.menu.value == 5:
            writevalue = '500'
        elif self.menu.value == 6:
            writevalue = '640'
        elif self.menu.value == 7:
            writevalue = '800'
        if self.menu.value != None:
            ConfigValues().cfgwrite('iso', writevalue)

        self.cfgvalue = str(ConfigValues().cfgread('iso'))
        self.info.value = self.parentApp.info_screen('ISO Setting: {}'.format(self.cfgvalue))
        self.info.display()
    
    def afterEditing(self):
        self.parentApp.setNextForm('Advanced')
        self.info = self.add(TextBox, name='Brightness Settings', editable=False, max_height=3*y // 8, value=self.parentApp.info_screen('Brightness Setting: {}'.format(self.cfgvalue)))
        self.nextrelx = 1
        self.endmark = self.add(npyscreen.FixedText, name='endmark', value='0', editable=False, max_height=y // 8, max_width=5, rely=y // 2)
        self.nextrelx = x // 2 - 1
        self.midmark = self.add(npyscreen.FixedText, name='midmark', value='50', editable=False, max_height=y // 8, max_width=5, rely=y // 2)
        self.nextrelx = -7
        self.endmark = self.add(npyscreen.FixedText, name='endmark', value='100', editable=False, max_height=y // 8, max_width=4, rely=y // 2)
        self.nextrelx = 2
        self.sliderval = self.add(npyscreen.SliderNoLabel, name='Brightness Info', max_height=y // 8, max_width=30*x // 32, value=int(self.cfgvalue), out_of=100)
    
    def adjust_widgets(self):
        ConfigValues().cfgwrite('brightness', str(int(self.sliderval.value)))
        self.cfgvalue = str(ConfigValues().cfgread('brightness'))
        self.info.value = self.parentApp.info_screen('Brightness Setting: {}'.format(self.cfgvalue))
        self.info.display()
    
    def afterEditing(self):
        self.parentApp.setNextForm('Advanced')

class QualityForm(npyscreen.Form):
    def create(self):
        y, x = self.useable_space()
        self.cfgvalue = str(ConfigValues().cfgread('quality'))

        self.info = self.add(TextBox, name='Quality Settings', editable=False, max_height=3*y // 8, value=self.parentApp.info_screen('Quality Setting: {}'.format(self.cfgvalue)))
        self.nextrelx = 1
        self.endmark = self.add(npyscreen.FixedText, name='endmark', value='0', editable=False, max_height=y // 8, max_width=5, rely=y // 2)
        self.nextrelx = x // 2 - 1
        self.midmark = self.add(npyscreen.FixedText, name='midmark', value='50', editable=False, max_height=y // 8, max_width=5, rely=y // 2)
        self.nextrelx = -7
        self.endmark = self.add(npyscreen.FixedText, name='endmark', value='100', editable=False, max_height=y // 8, max_width=4, rely=y // 2)
        self.nextrelx = 2
        self.sliderval = self.add(npyscreen.SliderNoLabel, name='Brightness Info', max_height=y // 8, max_width=30*x // 32, value=int(self.cfgvalue), out_of=100)
    
    def adjust_widgets(self):
        ConfigValues().cfgwrite('quality', str(int(self.sliderval.value)))
        self.cfgvalue = str(ConfigValues().cfgread('quality'))
        self.info.value = self.parentApp.info_screen('Quality Setting: {}'.format(self.cfgvalue))
        self.info.display()
    
    def afterEditing(self):
        self.parentApp.setNextForm('Advanced')

class EvForm(npyscreen.Form):
    def create(self):
        y, x = self.useable_space()
        self.cfgvalue = str(ConfigValues().cfgread('ev'))

        self.info = self.add(TextBox, name='EV Settings', editable=False, max_height=3*y // 8, value=self.parentApp.info_screen('EV Setting: {}'.format(self.cfgvalue)))
        self.nextrelx = 1
        self.endmark = self.add(npyscreen.FixedText, name='endmark', value='-25', editable=False, max_height=y // 8, max_width=5, rely=y // 2)
        self.nextrelx = x // 2 - 1
        self.midmark = self.add(npyscreen.FixedText, name='midmark', value='0', editable=False, max_height=y // 8, max_width=5, rely=y // 2)
        self.nextrelx = -7
        self.endmark = self.add(npyscreen.FixedText, name='endmark', value='25', editable=False, max_height=y // 8, max_width=4, rely=y // 2)
        self.nextrelx = 2
        self.sliderval = self.add(npyscreen.SliderNoLabel, name='EV Info', max_height=y // 8, max_width=30*x // 32, value=int(self.cfgvalue) + 25, out_of=50)
    
    def adjust_widgets(self):
        ConfigValues().cfgwrite('ev', str(int(self.sliderval.value) - 25))
        self.cfgvalue = str(ConfigValues().cfgread('ev'))
        self.info.value = self.parentApp.info_screen('EV Setting: {}'.format(self.cfgvalue))
        self.info.display()
    
    def afterEditing(self):
        self.parentApp.setNextForm('Advanced')


class IsoForm(npyscreen.Form):
    def create(self):
        y, x = self.useable_space()
        self.cfgvalue = str(ConfigValues().cfgread('iso'))
        self.info = self.add(TextBox, name='ISO Settings', editable=False, max_height=3*y // 8, value=self.parentApp.info_screen('ISO Setting: {}'.format(self.cfgvalue)))
        self.nextrely += 1
        self.menu = self.add(Menubox, name="ISO Setting Selection", max_height=y // 3, values=['Auto','100','200','320', '400', '500', '640', '800'], scroll_exit=True, contained_widget_arguments={'exit_right':True})
    
    def adjust_widgets(self):
        if self.menu.value == 0:
            writevalue = 'Auto'
        elif self.menu.value == 1:
            writevalue = '100'
        elif self.menu.value == 2:
            writevalue = '200'
        elif self.menu.value == 3:
            writevalue = '320'
        elif self.menu.value == 4:
            writevalue = '400'
        elif self.menu.value == 5:
            writevalue = '500'
        elif self.menu.value == 6:
            writevalue = '640'
        elif self.menu.value == 7:
            writevalue = '800'
        if self.menu.value != None:
            ConfigValues().cfgwrite('iso', writevalue)

        self.cfgvalue = str(ConfigValues().cfgread('iso'))
        self.info.value = self.parentApp.info_screen('ISO Setting: {}'.format(self.cfgvalue))
        self.info.display()
    
    def afterEditing(self):
        self.parentApp.setNextForm('Advanced')

# ---
class ExposureForm(npyscreen.Form):
    def create(self):
        y, x = self.useable_space()
        self.cfgvalue = str(ConfigValues().cfgread('exposure'))
        self.info = self.add(TextBox, name='Exposure Settings', editable=False, max_height=3*y // 8, value=self.parentApp.info_screen('Exposure Setting: {}'.format(self.cfgvalue)))
        self.nextrely += 1
        self.menu = self.add(Menubox, name='Exposure Setting Selection', max_height=y // 3, values=['Auto', 'Night', 'Night Preview', 'Backlight', 'Spotlight', 'Sports', 'Snow',
                             'Beach', 'Very Long', 'Fixed FPS', 'Anti-Shake', 'Fireworks'], scroll_exit=True, contained_widget_arguments={'exit_right': True})
    
    def adjust_widgets(self):
        if self.menu.value == 0:
            writevalue = 'Auto'
        elif self.menu.value == 1:
            writevalue = 'Night'
        elif self.menu.value == 2:
            writevalue = 'Night Preview'
        elif self.menu.value == 3:
            writevalue = 'Backlight'
        elif self.menu.value == 4:
            writevalue = 'Spotlight'
        elif self.menu.value == 5:
            writevalue = 'Sports'
        elif self.menu.value == 6:
            writevalue = 'Snow'
        elif self.menu.value == 7:
            writevalue = 'Beach'
        elif self.menu.value == 8:
            writevalue = 'Very Long'
        elif self.menu.value == 9:
            writevalue = 'Fixed FPS'
        elif self.menu.value == 10:
            writevalue = 'Anti-Shake'
        elif self.menu.value == 11:
            writevalue = 'Fireworks'
        if self.menu.value != None:
            ConfigValues().cfgwrite('exposure', writevalue)

        self.cfgvalue = str(ConfigValues().cfgread('exposure'))
        self.info.value = self.parentApp.info_screen('Exposure Setting: {}'.format(self.cfgvalue))
        self.info.display()
    
    def afterEditing(self):
        self.parentApp.setNextForm('Advanced')
        self.info.display()
    
class WhitebalanceForm(npyscreen.Form):
    def create(self):
        y, x = self.useable_space()
        self.cfgvalue = str(ConfigValues().cfgread('whitebalance'))
        self.info = self.add(TextBox, name='Auto White Balance Setting', editable=False, max_height=3*y // 8, value=self.parentApp.info_screen('Auto White Balance Setting: {}'.format(self.cfgvalue)))
        self.nextrely += 1
        self.menu = self.add(Menubox, name='Auto White Balance Selection', max_height=y // 3, values=['Off', 'Auto', 'Sun', 'Cloud', 'Shade', 'Tungsten', 'Flourescent',
                             'Incandescent', 'Flash', 'Horizon'], scroll_exit=True, contained_widget_arguments={'exit_right': True})
    
    def adjust_widgets(self):
        if self.menu.value == 0:
            writevalue = 'Off'
        elif self.menu.value == 1:
            writevalue = 'Auto'
        elif self.menu.value == 2:
            writevalue = 'Sun'
        elif self.menu.value == 3:
            writevalue = 'Cloud'
        elif self.menu.value == 4:
            writevalue = 'Shade'
        elif self.menu.value == 5:
            writevalue = 'Tungsten'
        elif self.menu.value == 6:
            writevalue = 'Flourescent'
        elif self.menu.value == 7:
            writevalue = 'Incandescent'
        elif self.menu.value == 8:
            writevalue = 'Flash'
        elif self.menu.value == 9:
            writevalue = 'Horizon'
        if self.menu.value != None:
            ConfigValues().cfgwrite('whitebalance', writevalue)

        self.cfgvalue = str(ConfigValues().cfgread('whitebalance'))
        self.info.value = self.parentApp.info_screen('Auto White Balance Setting: {}'.format(self.cfgvalue))
        self.info.display()
    
    def afterEditing(self):
        self.parentApp.setNextForm('Advanced')
        self.info.display()

class FiletypeForm(npyscreen.Form):
    def create(self):
        y, x = self.useable_space()
        self.cfgvalue = str(ConfigValues().cfgread('filetype'))
        self.info = self.add(TextBox, name='Saved Filetype Setting', editable=False, max_height=3*y // 8, value=self.parentApp.info_screen('Saved File Type Setting: {}'.format(self.cfgvalue)))
        self.nextrely += 1
        self.menu = self.add(Menubox, name='Saved Filetype Selection', max_height=y // 3, values=['JPEG', 'BMP', 'GIF', 'PNG'], 
                             scroll_exit=True, contained_widget_arguments={'exit_right': True})
    
    def adjust_widgets(self):
        if self.menu.value == 0:
            writevalue = 'JPEG'
        elif self.menu.value == 1:
            writevalue = 'BMP'
        elif self.menu.value == 2:
            writevalue = 'GIF'
        elif self.menu.value == 3:
            writevalue = 'PNG'
        if self.menu.value != None:
            ConfigValues().cfgwrite('filetype', writevalue)

        self.cfgvalue = str(ConfigValues().cfgread('filetype'))
        self.info.value = self.parentApp.info_screen('Saved File Type Setting: {}'.format(self.cfgvalue))
        self.info.display()
    
    def afterEditing(self):
        self.parentApp.setNextForm('Advanced')
        self.info.display()

class LedOneForm(npyscreen.Form):
    def create(self):
        y, x = self.useable_space()
        self.cfgvalue = ConfigValues().cfgread('ledone')
        if self.cfgvalue == 'On':
            ledstatus = 0
        else:
            ledstatus = 1


        self.info = self.add(TextBox, name='1st LED Settings', editable=False, max_height=3*y // 8, value=self.parentApp.info_screen('1st LED status: {}'.format(self.cfgvalue)))
        self.nextrely += 1
        self.menu = self.add(Menubox, name='Set LED status', relx=4, max_height=y // 4, max_width=x // 3, value=ledstatus,
                             values=['On', 'Off'], scroll_exit=True, contained_widget_arguments={'exit_right':True})
    
    def adjust_widgets(self):
        if self.menu.value == 0:
            writevalue = 'On'
        elif self.menu.value == 1:
            writevalue = 'Off'
        if self.menu.value != None:
            ConfigValues().cfgwrite('ledone', writevalue)

        self.cfgvalue = str(ConfigValues().cfgread('ledone'))
        self.info.value = self.parentApp.info_screen('1st LED Status: {}'.format(self.cfgvalue))
        self.info.display()
    
    def afterEditing(self):
        self.parentApp.setNextForm('Peripherals')
        self.info.display()

class LedTwoForm(npyscreen.Form):
    def create(self):
        y, x = self.useable_space()
        self.cfgvalue = ConfigValues().cfgread('ledtwo')
        if self.cfgvalue == 'On':
            ledstatus = 0
        else:
            ledstatus = 1


        self.info = self.add(TextBox, name='2nd LED Settings', editable=False, max_height=3*y // 8, value=self.parentApp.info_screen('2nd LED status: {}'.format(self.cfgvalue)))
        self.nextrely += 1
        self.menu = self.add(Menubox, name='Set LED status', relx=4, max_height=y // 4, max_width=x // 3, value=ledstatus,
                             values=['On', 'Off'], scroll_exit=True, contained_widget_arguments={'exit_right':True})
    
    def adjust_widgets(self):
        if self.menu.value == 0:
            writevalue = 'On'
        elif self.menu.value == 1:
            writevalue = 'Off'
        if self.menu.value != None:
            ConfigValues().cfgwrite('ledtwo', writevalue)

        self.cfgvalue = str(ConfigValues().cfgread('ledtwo'))
        self.info.value = self.parentApp.info_screen('2nd LED Status: {}'.format(self.cfgvalue))
        self.info.display()
    
    def afterEditing(self):
        self.parentApp.setNextForm('Peripherals')
        self.info.display()


class CycletimeForm(npyscreen.Form):
    def create(self):
        y, x = self.useable_space()
        self.cfgvalue = self.parentApp.convertms(int(ConfigValues().cfgread('cycletime')))
        if int(ConfigValues().cfgread('cycletime')) <= 0:
            self.cfgvalue = 'Forever Until Shutoff'

        self.info = self.add(TextBox, name='Cycletime Settings', editable=False, max_height=3*y // 8, value=self.parentApp.info_screen('Timelapse Length: {}'.format(self.cfgvalue)))
        self.nextrely += 1
        self.timeinterval = self.add(Menubox, name='Set Time Increments', relx=4, max_height=y // 4, max_width=x // 3, value=1,
                             values=['Miliseconds','Seconds', 'Minutes', 'Hours', 'Days'], scroll_exit=True, contained_widget_arguments={'exit_right':True})
        self.nextrelx = 2
        self.sliderval = self.add(Sliderbox, name='Use Side Arrow Keys To Adjust Value', max_width=2*x // 3 - 10, max_height=3, contained_widget_arguments={'value':int(ConfigValues().cfgread('cycletime')), 'step':1000, 'out_of':float('inf')}, rely=y // 2, relx=x // 3 + 5, scroll_exit=True)
    
    def adjust_widgets(self):
        ConfigValues().cfgwrite('cycletime', str(int(self.sliderval.value)))
        self.cfgvalue = str(ConfigValues().cfgread('cycletime'))
        if self.timeinterval.value != None:
            if self.timeinterval.value == 0:
                self.sliderval.entry_widget.step = 10
            if self.timeinterval.value == 1:
                self.sliderval.entry_widget.step = 1000
            if self.timeinterval.value == 2:
                self.sliderval.entry_widget.step = 60000
            if self.timeinterval.value == 3:
                self.sliderval.entry_widget.step = 3600000
            if self.timeinterval.value == 4:
                self.sliderval.entry_widget.step = 86400000
        if int(ConfigValues().cfgread('cycletime')) <= 0:
            self.info.value = self.parentApp.info_screen('Timelapse Length: Forever Until Shutoff')
        else:
            self.info.value = self.parentApp.info_screen('Timelapse Length: {}'.format(self.parentApp.convertms(self.cfgvalue)))
        self.info.display()
    
    def afterEditing(self):
        self.parentApp.setNextForm('Preferences')

class FrequencyForm(npyscreen.Form):
    def create(self):
        y, x = self.useable_space()
        self.cfgvalue = self.parentApp.convertms(int(ConfigValues().cfgread('frequency')))
        if int(ConfigValues().cfgread('frequency')) <= 30:
            self.cfgvalue = 'As Fast As Possible' 

        self.info = self.add(TextBox, name='Frequency Settings', editable=False, max_height=3*y // 8, value=self.parentApp.info_screen('Time Between Photos: {}'.format(self.cfgvalue)))
        self.nextrely += 1
        self.timeinterval = self.add(Menubox, name='Set Time Increments', relx=4, max_height=y // 4, max_width=x // 3, value=1,
                             values=['Miliseconds','Seconds', 'Minutes', 'Hours', 'Days'], scroll_exit=True, contained_widget_arguments={'exit_right':True})
        self.nextrelx = 2
        self.sliderval = self.add(Sliderbox, name='Use Side Arrow Keys To Adjust Value', max_width=2*x // 3 - 10, max_height=3, contained_widget_arguments={'lowest':0, 'value':int(ConfigValues().cfgread('frequency')), 'step':1000, 'out_of':float('inf')}, rely=y // 2, relx=x // 3 + 5, scroll_exit=True)
    
    def adjust_widgets(self):
        ConfigValues().cfgwrite('frequency', str(int(self.sliderval.value)))
        if self.timeinterval.value != None:
            if self.timeinterval.value == 0:
                self.sliderval.entry_widget.step = 10
            if self.timeinterval.value == 1:
                self.sliderval.entry_widget.step = 1000
            if self.timeinterval.value == 2:
                self.sliderval.entry_widget.step = 60000
            if self.timeinterval.value == 3:
                self.sliderval.entry_widget.step = 3600000
            if self.timeinterval.value == 4:
                self.sliderval.entry_widget.step = 86400000
        if self.sliderval.value <= 30:
            self.info.value = self.parentApp.info_screen('Time Between Photos: As Fast As Possible')
        else:
            self.cfgvalue = str(ConfigValues().cfgread('frequency'))
            self.info.value = self.parentApp.info_screen('Time Between Photos: {}'.format(self.parentApp.convertms(self.cfgvalue)))
        self.info.display()
    
    def afterEditing(self):
        self.parentApp.setNextForm('Preferences')

class InfoForm(npyscreen.Form, ConfigValues):
    def writescreen(self):
        cfgvals = ConfigValues()
        if int(cfgvals.cfgread('cycletime')) > 0:
            cycletime = cfgvals.cfgread('cycletime')
        else:
            cycletime = 'Forever Until Shutdown'
        if int(cfgvals.cfgread('frequency')) > 30:
            frequency = self.parentApp.convertms(cfgvals.cfgread('frequency'))
        else:
            frequency = 'As Fast As Possible'
        screen = ['   _______      _______  _____  ',
                  '  / ____\ \    / /_   _|/ ____| ',
                  ' | (___  \ \  / /  | | | (___   ',
                  '  \___ \  \ \/ /   | |  \___ \  ',
                  '  ____) |  \  /   _| |_ ____) | ',
                  ' |_____/    \/   |_____|_____/  ',
                  '                                ',
                  ' Sequential Vegitation          ',
                  ' Imaging System                 ',
                  '                                ',
                  ' Cycletime: {}'.format(cycletime),
                  ' Frequency: {}'.format(frequency),
                  ' Exposure: {}'.format(cfgvals.cfgread('exposure')),
                  ' ISO: {}'.format(cfgvals.cfgread('iso')),
                  ' Sharpness: {}'.format(cfgvals.cfgread('sharpness')),
                  ' Saturation: {}'.format(cfgvals.cfgread('saturation')),
                  ' Brightness: {}'.format(cfgvals.cfgread('brightness')),
                  ' Contrast: {}'.format(cfgvals.cfgread('contrast')),
                  ' EV: {}'.format(cfgvals.cfgread('ev')),
                  ' Quality: {}'.format(cfgvals.cfgread('quality')),
                  ' White Balance: {}'.format(cfgvals.cfgread('whitebalance')),
                  ' File Type: {}'.format(cfgvals.cfgread('filetype')),
                  ' 1st LED Status: {}'.format(cfgvals.cfgread('ledone')),
                  ' 2nd LED Status: {}'.format(cfgvals.cfgread('ledtwo'))]
        return screen
    def create(self):
        self.info = self.add(Menubox, name='Camera Preferences', values=self.writescreen(), color='STANDOUT', contained_widget_arguments={'exit_right': True, 'color':'GOOD'})
    def afterEditing(self):
        self.parentApp.setNextForm('MAIN')

class StartForm(npyscreen.Form):
    def create(self):
        self.keypress_timeout = 1
        self.info = self.add(Menubox, name='Camera Preferences', values=['Timelapse in progress', 'Hit enter to stop manually'], color='STANDOUT', contained_widget_arguments={'exit_right': True, 'color':'GOOD'})
    
    def beforeEditing(self):
        StartTimelapse().start()
        self.info.values = os.listdir('./photos')
    
    def afterEditing(self):
        self.parentApp.setNextForm('MAIN')



try:
    MyApp = App()
    MyApp.run()
except KeyboardInterrupt:
    print()